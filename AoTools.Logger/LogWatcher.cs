﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

namespace AoTools.Logger
{
    public class LogWatcher
    {
        public const int REFRESH_MS = 1000; //* 120; //At Each 2 Minutes

        public String Path { get; private set; }
        public ItemList LoggedItem { get; private set; }
        public DateTime LastUpdate { get; private set; }
        public LogFile logFile { get; private set; }
        private int RefreshTime;

        private bool AskStop=false;

        public LogWatcher(String Path)
        {
            LoggedItem = new ItemList();
            LastUpdate = new DateTime();
            logFile = new LogFile(Path);
            this.Path = Path;
            RefreshTime = REFRESH_MS;
        }

        public void ReloadFile()
        {
            logFile.LoadFile();
        }

        public void AskToStop()
        {
            AskStop = true;
        }

        public void Watch()
        {
            AskStop = false;
            while (!AskStop)
            {
                ReloadFile();

                ItemList tmp = Item.ParseItems(logFile);

                LoggedItem.AddList(tmp);
                LastUpdate = DateTime.Now;

                Thread.Sleep(RefreshTime);
            }
        }
    }
}
