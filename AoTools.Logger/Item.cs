﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace AoTools.Logger
{
    public enum ItemType { White, Normal, Magic, Rare, Unique, Misc, Superior, Unknown }

    public class ItemList : List<Item>
    {
        public ItemList()
            : base()
        {

        }

        public bool ContainsItem(Item Item)
        {
            foreach (Item i in this)
            {
                if (i.Equals(Item))
                {
                    return true;
                }
            }

            return false;
        }

        public void AddList(ItemList List)
        {
            foreach (Item i in List)
            {
                if (!ContainsItem(i))
                {
                    Add(i);
                }
            }
        }

        public void AddItem(Item Item)
        {
            if (!ContainsItem(Item))
            {
                Add(Item);
            }
        }
    }

    public class Item
    {
        public String Name { get; set; }
        public ItemType iType { get; set; }
        public uint ReqLevel { get; set; }
        public LogEntryId LogEntry { get; set; }
        public List<String> Stats { get; set; }
        public String FoundAt { get; set; }

        public Item()
        {
            Name = "None";
            iType = ItemType.Unknown;
            ReqLevel = 0;
            Stats = new List<String>();
        }

        public override string ToString()
        {
            return "Name : " + Name + ", iType : " + iType + ", ReqLevel : " + ReqLevel;
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Item)
            {
                Item item = (Item)obj;

                if (item.Name == Name && item.LogEntry.Equals(LogEntry) && item.iType == iType)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public String GetStrType()
        {
            if (iType == ItemType.Normal)
            {
                return "Normal";
            }
            else if (iType == ItemType.White)
            {
                return "White";
            }
            else if (iType == ItemType.Magic)
            {
                return "Magic";
            }
            else if (iType == ItemType.Rare)
            {
                return "Rare";
            }
            else if (iType == ItemType.Unique)
            {
                return "Unique";
            }
            else if (iType == ItemType.Misc)
            {
                return "Misc";
            }
            else if (iType == ItemType.Superior)
            {
                return "Superior";
            }
            else
            {
                return "Unknown";
            }
        }

        public static Item ParseItemStat(String ItemString)
        {
            String[] Splitted = ItemString.Split('\n');

            foreach (String s in Splitted)
            {
                Console.WriteLine(s + " n: " + s.Length);
            }

            return new Item();
        }

        public static ItemList ParseItems(LogFile logFile)
        {
            ItemList Items = new ItemList();
            String[] Splitted = logFile.Content.Split('\n');

            Item tmpItem = new Item();

            for (int i = 0; i < Splitted.Length; i++ )
            {
                String str = Splitted[i];
                if (str.StartsWith("[") && str.Contains("]"))
                {
                    tmpItem = new Item();
                    tmpItem.LogEntry = new LogEntryId(logFile, i);
                    String[] tmp = str.Split(']');
                    String iType = tmp[0];
                    String iName = tmp[1];
                    iType = iType.Substring(1, iType.Length - 1);

                    iType = iType.Substring(1, iType.Length - 1);
                    iType = iType.Substring(0, iType.Length - 1);

                    if (iType == "Misc")
                    {
                        tmpItem.iType = ItemType.Misc;
                    }
                    else if (iType == "White")
                    {
                        tmpItem.iType = ItemType.White;
                    }
                    else if (iType == "Magic")
                    {
                        tmpItem.iType = ItemType.Magic;
                    }
                    else if (iType == "Rare")
                    {
                        tmpItem.iType = ItemType.Rare;
                    }
                    else if (iType == "Unique")
                    {
                        tmpItem.iType = ItemType.Unique;
                    }
                    else if (iType == "White")
                    {
                        tmpItem.iType = ItemType.White;
                    }
                    else if (iType == "Normal")
                    {
                        tmpItem.iType = ItemType.Normal;
                    }
                    else if (iType == "Superior")
                    {
                        tmpItem.iType = ItemType.Superior;
                    }
                    else
                    {
                        tmpItem.iType = ItemType.Unknown;
                    }

                    while (iName.StartsWith(" "))
                    {
                        iName = iName.Substring(1);
                    }

                    iName = iName.Replace(" found at: ", "|");
                    String[] tmp2 = iName.Split('|');

                    tmpItem.Name = tmp2[0];
                    tmpItem.FoundAt = tmp2[1].Substring(0,tmp2[1].Length-1);
                }
                else if (str.Contains('\t'))
                {
                    //Maybe a stats
                    String tStat = str.Substring(3);
                    if (tStat[0] == '\r')
                    {
                        //New Item
                        Items.AddItem(tmpItem);
                    }
                    else
                    {
                        if (tStat.StartsWith("Required Level:"))
                        {

                            String sReqLvl = tStat.Substring(16, 2);
                            tmpItem.ReqLevel = uint.Parse(sReqLvl);
                        }
                        else
                        {
                            tStat = tStat.Substring(0, tStat.Length - 1);
                            tmpItem.Stats.Add(tStat);
                        }
                    }


                }
            }

            return Items;

        }
    }
}
