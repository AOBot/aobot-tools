﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace AoTools.Logger
{
    public class LogEntryId
    {
        public DateTime DateCreated { get; set; }
        public String Filename { get; set; }
        public int Line { get; set; }

        public LogEntryId()
        {
            DateCreated = new DateTime();
            Filename = "none";
            Line = -1;
        }

        public LogEntryId(LogFile Log, int Line)
        {
            DateCreated = Log.DateCreated;
            Filename = Log.Filename;
            this.Line = Line;

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is LogEntryId)
            {
                LogEntryId e = (LogEntryId)obj;

                if (e.DateCreated.Equals(DateCreated) && Line == e.Line && e.Filename == Filename)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
    }

    public class LogFile
    {
        public DateTime DateCreated { get; private set; }
        public String Filename { get; private set; }
        public long Length { get; private set; }
        public String Content { get; private set; }

        public LogFile()
        {
            Filename = "none";
            DateCreated = new DateTime();
            Length = 0;
        }

        public LogFile(String Path)
        {
            Filename = Path;
            LoadFile();
        }

        public void LoadFile()
        {
            if (!File.Exists(Filename))
            {
                Filename = Filename;
                DateCreated = new DateTime();
                Content = "";
                Length = 0;
            }
            else
            {
                DateCreated = File.GetCreationTime(Filename);
                FileStream Stream = new FileStream(Filename, FileMode.Open);
                Filename = Stream.Name;
                Length = Stream.Length;
                TextReader reader = new StreamReader(Stream);
                Content = reader.ReadToEnd();

                Stream.Close();
            }
        }
    }
}
