﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;

using System.Net;
using System.Net.Sockets;
using System.IO;

using AoTools.Logger;

namespace AoTools.Network
{
    public class AoHttpServer : HttpServer
    {

        public LogWatcher StashedLog;
        public LogWatcher SoldLog;

        public Thread StashedLogThread;
        public Thread SoldLogThread;

        public AoServerSettings Settings;
     
        public AoHttpServer(AoServerSettings Settings)
            : base(Settings.Port)
        {
            this.Settings = Settings;

            StashedLog = new LogWatcher(Settings.LogDir + "Stashed.log");
            StashedLogThread = new Thread(new ThreadStart(StashedLog.Watch));

            SoldLog = new LogWatcher(Settings.LogDir + "Sold.log");
            SoldLogThread = new Thread(new ThreadStart(SoldLog.Watch));

            StashedLogThread.Start();
            SoldLogThread.Start();
        }

        public String StashedItemPage()
        {
            TextReader reader = new StreamReader("Html/itempage.html");
            String Content = reader.ReadToEnd();
            
            reader.Close();

            Content = Content.Replace("%ITEM_START%","[");
            Content = Content.Replace("%ITEM_END%", "]");

            String[] Tmp = Content.Split('[');
            String[] Tmp2 = Tmp[1].Split(']');

           

            String Header = Tmp[0];
            String Footer = Tmp2[1];
            String ItemDiv = Tmp2[0];

            Header = Header.Replace("%TITLE%", "AoTools : Bot Status");
            Header = Header.Replace("%HEADER%", "AoTools : Bot Status");
            Header = Header.Replace("%ITEM_STASHED%", StashedLog.LoggedItem.Count + " Item(s) Stashed");

            String Items = "";

            foreach(Item i in StashedLog.LoggedItem)
            {
                String TmpDiv = ItemDiv;

                TmpDiv = TmpDiv.Replace("%ITEM_NAME%", i.Name);
                TmpDiv = TmpDiv.Replace("%ITEM_TYPE%", i.GetStrType());
                TmpDiv = TmpDiv.Replace("%ITEM_FOUNDAT%", i.FoundAt);

                String Stats ="";

                foreach(String s in i.Stats)
                {
                    Stats += s + "<br />";
                }

                TmpDiv = TmpDiv.Replace("%ITEM_STATS%",Stats);

                Items += TmpDiv;
            }


            Footer = Footer.Replace("%FOOTER%", "AoTools-0.1");


            return (Header + Items + Footer);

        }

        public String SoldItemPage()
        {
            TextReader reader = new StreamReader("Html/itempage.html");
            String Content = reader.ReadToEnd();

            reader.Close();

            Content = Content.Replace("%ITEM_START%", "[");
            Content = Content.Replace("%ITEM_END%", "]");

            String[] Tmp = Content.Split('[');
            String[] Tmp2 = Tmp[1].Split(']');



            String Header = Tmp[0];
            String Footer = Tmp2[1];
            String ItemDiv = Tmp2[0];

            Header = Header.Replace("%TITLE%", "AoTools : Bot Status");
            Header = Header.Replace("%HEADER%", "AoTools : Bot Status");
            Header = Header.Replace("%ITEM_STASHED%", SoldLog.LoggedItem.Count + " Item(s) Solded");

            String Items = "";

            foreach (Item i in SoldLog.LoggedItem)
            {
                String TmpDiv = ItemDiv;

                TmpDiv = TmpDiv.Replace("%ITEM_NAME%", i.Name);
                TmpDiv = TmpDiv.Replace("%ITEM_TYPE%", i.GetStrType());
                TmpDiv = TmpDiv.Replace("%ITEM_FOUNDAT%", i.FoundAt);

                String Stats = "";

                foreach (String s in i.Stats)
                {
                    Stats += s + "<br />";
                }

                TmpDiv = TmpDiv.Replace("%ITEM_STATS%", Stats);

                Items += TmpDiv;
            }


            Footer = Footer.Replace("%FOOTER%", "AoTools-0.1");


            return (Header + Items + Footer);

        }

        public override void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp)
        {
            string path = rq.URL.Replace("/", "\\");

            Console.WriteLine(path);
            if (rq.URL == "/" || rq.URL == "/items")
            {
                rp.BodyData = Encoding.ASCII.GetBytes(StashedItemPage());
                return;
            }
            else if (rq.URL == "/sold")
            {
                rp.BodyData = Encoding.ASCII.GetBytes(SoldItemPage());
                return;
            }

            /*
            if (Directory.Exists(path))
            {
                if (File.Exists(path + "index.html"))
                    path += "\\index.html";
                else
                {
                    string[] dirs = Directory.GetDirectories(path);
                    string[] files = Directory.GetFiles(path);

                    string bodyStr = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
                    bodyStr += "<HTML><HEAD>\n";
                    bodyStr += "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n";
                    bodyStr += "</HEAD>\n";
                    bodyStr += "<BODY><p>AoTools Http Server</p>\n";
                    for (int i = 0; i < dirs.Length; i++)
                        bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(dirs[i]) + "/\">[" + Path.GetFileName(dirs[i]) + "]</a>\n";
                    for (int i = 0; i < files.Length; i++)
                        bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(files[i]) + "\">" + Path.GetFileName(files[i]) + "</a>\n";
                    bodyStr += "</BODY></HTML>\n";

                    rp.BodyData = Encoding.ASCII.GetBytes(bodyStr);
                    return;
                }
            }

            if (File.Exists(path))
            {
                //RegistryKey rk = Registry.ClassesRoot.OpenSubKey(Path.GetExtension(path), true);

                // Get the data from a specified item in the key.
                //String s = (String)rk.GetValue("Content Type");

                String s = MimeType.GetType(path);
                // Open the stream and read it back.
                rp.fs = File.Open(path, FileMode.Open);
                if (s != "")
                    rp.Headers["Content-type"] = s;
            }*/
            else
            {

                rp.status = (int)RespState.NOT_FOUND;

                string bodyStr = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
                bodyStr += "<HTML><HEAD>\n";
                bodyStr += "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n";
                bodyStr += "</HEAD>\n";
                bodyStr += "<BODY>File not found!!</BODY></HTML>\n";

                rp.BodyData = Encoding.ASCII.GetBytes(bodyStr);

            }
            
        }
    }
}
