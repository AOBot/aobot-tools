﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using System.Xml;
using System.Xml.Serialization;

namespace AoTools.Network
{
    public class AoServerSettings
    {
        [XmlAttribute("LogDir")] public String LogDir { get; set; }
        [XmlAttribute("Port")] public int Port { get; set; }
        [XmlAttribute("CharName")] public String CharName { get; set; }

        public AoServerSettings()
        {
            LogDir = "/";
            Port = 8080;
            CharName = "unset";
        }

        public static void SaveSettings(AoServerSettings Settings, String Path)
        {
            XmlSerializer Xs = new XmlSerializer(typeof(AoServerSettings));

            TextWriter writer = new StreamWriter(Path);

            Xs.Serialize(writer, Settings);

            writer.Close();
        }

        public static AoServerSettings LoadSettings(String Path)
        {
            AoServerSettings Settings = new AoServerSettings();

            XmlSerializer Xs = new XmlSerializer(typeof(AoServerSettings));

            TextReader reader = new StreamReader(Path);

            Settings = (AoServerSettings)Xs.Deserialize(reader);

            reader.Close();

            return Settings;
        }
    }
}
