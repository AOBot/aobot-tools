﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using System.Threading;

using System.Net;
using System.Net.Sockets;
using System.IO;


namespace AoTools.Network
{

    public class ClassicHttpServer : HttpServer
    {
        public string Folder;

        public ClassicHttpServer()
            : base()
        {
            this.Folder = "c:\\www";
        }

        public ClassicHttpServer(int thePort, string theFolder)
            : base(thePort)
        {
            this.Folder = theFolder;
        }

        public override void OnResponse(ref HTTPRequestStruct rq, ref HTTPResponseStruct rp)
        {
            string path = this.Folder + "\\" + rq.URL.Replace("/", "\\");
            
            if (Directory.Exists(path))
            {
                if (File.Exists(path + "index.html"))
                    path += "\\index.html";
                else
                {
                    string[] dirs = Directory.GetDirectories(path);
                    string[] files = Directory.GetFiles(path);

                    string bodyStr = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
                    bodyStr += "<HTML><HEAD>\n";
                    bodyStr += "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n";
                    bodyStr += "</HEAD>\n";
                    bodyStr += "<BODY><p>AoTools Http Server</p>\n";
                    for (int i = 0; i < dirs.Length; i++)
                        bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(dirs[i]) + "/\">[" + Path.GetFileName(dirs[i]) + "]</a>\n";
                    for (int i = 0; i < files.Length; i++)
                        bodyStr += "<br><a href = \"" + rq.URL + Path.GetFileName(files[i]) + "\">" + Path.GetFileName(files[i]) + "</a>\n";
                    bodyStr += "</BODY></HTML>\n";

                    rp.BodyData = Encoding.ASCII.GetBytes(bodyStr);
                    return;
                }
            }

            if (File.Exists(path))
            {
                /*RegistryKey rk = Registry.ClassesRoot.OpenSubKey(Path.GetExtension(path), true);

                // Get the data from a specified item in the key.
                String s = (String)rk.GetValue("Content Type");*/

                String s = MimeType.GetType(path);
                // Open the stream and read it back.
                rp.fs = File.Open(path, FileMode.Open);
                if (s != "")
                    rp.Headers["Content-type"] = s;
            }
            else
            {

                rp.status = (int)RespState.NOT_FOUND;

                string bodyStr = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
                bodyStr += "<HTML><HEAD>\n";
                bodyStr += "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n";
                bodyStr += "</HEAD>\n";
                bodyStr += "<BODY>File not found!!</BODY></HTML>\n";

                rp.BodyData = Encoding.ASCII.GetBytes(bodyStr);

            }

        }
    }

    public abstract class HttpServer
    {
        private int portNum = 8090;
        private TcpListener listener;
        Thread ListenerThread;

        public Hashtable respStatus;

        public string Name = "ao-bot-tools-httpd/0.1";

        public bool IsAlive
        {
            get
            {
                return this.ListenerThread.IsAlive;
            }
        }

        public HttpServer()
        {
            respStatusInit();
        }

        public HttpServer(int Port)
        {
            portNum = Port;
            respStatusInit();
        }

        private void respStatusInit()
        {
            respStatus = new Hashtable();

            respStatus.Add(200, "200 Ok");
            respStatus.Add(201, "201 Created");
            respStatus.Add(202, "202 Accepted");
            respStatus.Add(204, "204 No Content");

            respStatus.Add(301, "301 Moved Permanently");
            respStatus.Add(302, "302 Redirection");
            respStatus.Add(304, "304 Not Modified");

            respStatus.Add(400, "400 Bad Request");
            respStatus.Add(401, "401 Unauthorized");
            respStatus.Add(403, "403 Forbidden");
            respStatus.Add(404, "404 Not Found");

            respStatus.Add(500, "500 Internal Server Error");
            respStatus.Add(501, "501 Not Implemented");
            respStatus.Add(502, "502 Bad Gateway");
            respStatus.Add(503, "503 Service Unavailable");
        }

        public void Listen()
        {
            bool done = false;
            listener = new TcpListener(portNum);
            listener.Start();

            Console.WriteLine("Listeninig...");

            while (!done)
            {
                Console.WriteLine("Waiting connections...");
                HttpRequest newRequest = new HttpRequest(listener.AcceptTcpClient(), this);

                Thread newThread = new Thread(new ThreadStart(newRequest.Process));
                newThread.Name = "HTTP Request";
                newThread.Start();
            }
        }

        public void WriteLog(String msg)
        {
            Console.WriteLine(msg);
        }
        public void Start()
        {
            // CSHTTPServer HTTPServer = new CSHTTPServer(portNum);

            this.ListenerThread = new Thread(new ThreadStart(this.Listen));
            this.ListenerThread.Start();
        }

        public void Stop()
        {
            listener.Stop();
            this.ListenerThread.Abort();
        }

        public void Suspend()
        {
            this.ListenerThread.Suspend();
        }

        public void Resume()
        {
            this.ListenerThread.Resume();
        }

        public abstract void OnResponse(ref HTTPRequestStruct rq,
                                        ref HTTPResponseStruct rp);

    }
}
