﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

using AoTools.Network;

namespace AoTools.Main
{
    public class Apps
    {
        public static void Main(String[] args)
        {
            AoServerSettings Settings;
            if (File.Exists("settings.xml"))
            {
                Settings = AoServerSettings.LoadSettings("settings.xml");
            }
            else
            {
                Settings = new AoServerSettings();
                AoServerSettings.SaveSettings(Settings, "settings.xml");
            }

            AoHttpServer serv = new AoHttpServer(Settings);
            serv.Start();
        }
    }
}
