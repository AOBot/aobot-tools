﻿namespace AoTools.LoggerGui
{
    partial class ItemLogWatcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemDataGrid = new System.Windows.Forms.DataGridView();
            this.RefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.logFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.listStats = new System.Windows.Forms.ListBox();
            this.lItemStats = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemDataGrid
            // 
            this.ItemDataGrid.AllowUserToAddRows = false;
            this.ItemDataGrid.AllowUserToDeleteRows = false;
            this.ItemDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ItemDataGrid.Location = new System.Drawing.Point(12, 26);
            this.ItemDataGrid.MultiSelect = false;
            this.ItemDataGrid.Name = "ItemDataGrid";
            this.ItemDataGrid.ReadOnly = true;
            this.ItemDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ItemDataGrid.Size = new System.Drawing.Size(646, 436);
            this.ItemDataGrid.TabIndex = 0;
            this.ItemDataGrid.SelectionChanged += new System.EventHandler(this.ItemDataGrid_SelectionChanged);
            // 
            // RefreshTimer
            // 
            this.RefreshTimer.Tick += new System.EventHandler(this.RefreshTimer_Tick);
            // 
            // listStats
            // 
            this.listStats.FormattingEnabled = true;
            this.listStats.Location = new System.Drawing.Point(664, 42);
            this.listStats.Name = "listStats";
            this.listStats.ScrollAlwaysVisible = true;
            this.listStats.Size = new System.Drawing.Size(205, 199);
            this.listStats.TabIndex = 1;
            // 
            // lItemStats
            // 
            this.lItemStats.AutoSize = true;
            this.lItemStats.Location = new System.Drawing.Point(665, 26);
            this.lItemStats.Name = "lItemStats";
            this.lItemStats.Size = new System.Drawing.Size(52, 13);
            this.lItemStats.TabIndex = 2;
            this.lItemStats.Text = "Item stats";
            // 
            // ItemLogWatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 468);
            this.Controls.Add(this.lItemStats);
            this.Controls.Add(this.listStats);
            this.Controls.Add(this.ItemDataGrid);
            this.Name = "ItemLogWatcher";
            this.Text = "Item Log Watcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItemLog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ItemDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ItemDataGrid;
        private System.Windows.Forms.Timer RefreshTimer;
        private System.Windows.Forms.FolderBrowserDialog logFolderDialog;
        private System.Windows.Forms.ListBox listStats;
        private System.Windows.Forms.Label lItemStats;
    }
}

