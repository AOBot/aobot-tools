﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

using AoTools.Logger;

namespace AoTools.LoggerGui
{
    public partial class ItemLogWatcher : Form
    {
        private LogWatcher Watcher;
        private DataTable ItemDataTable;
        private Thread WatcherThread;

        private int CurItemIndex=0;
        public int RefreshTime = 1000;
        public int SelectedItemIndex = -1;

        public ItemLogWatcher()
        {
            InitializeComponent();

            logFolderDialog.ShowDialog();
            Watcher = new LogWatcher(logFolderDialog.SelectedPath + "/Stashed.log");

            ItemDataTable = new DataTable();
            InitDataSource();

            ItemDataGrid.DataSource = ItemDataTable;


            WatcherThread = new Thread(Watcher.Watch);

            WatcherThread.Start();

            ItemDataTable.AcceptChanges();

            ItemDataGrid.Update();

            RefreshTimer.Interval = RefreshTime;

            RefreshTimer.Start();
        }

        public void FillDataSource()
        {
            for (int i = CurItemIndex; i < Watcher.LoggedItem.Count; i++)
            {
                Item item = Watcher.LoggedItem[i];
                ItemDataTable.Rows.Add(item.Name, item.GetStrType(), item.ReqLevel, item.Stats.Count, item.FoundAt);
                CurItemIndex++;
            }

        }

        public void InitDataSource()
        {
            ItemDataTable.Columns.Add("Name", typeof(String));
            ItemDataTable.Columns.Add("Type", typeof(String));
            ItemDataTable.Columns.Add("Req Lvl", typeof(int));
            ItemDataTable.Columns.Add("Stats", typeof(int));
            ItemDataTable.Columns.Add("Found At", typeof(String));
        }

        private void ItemLog_FormClosing(object sender, FormClosingEventArgs e)
        {
            RefreshTimer.Stop();

            Watcher.AskToStop();

        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            FillDataSource();
            ItemDataGrid.Update();
            ItemStatsRefresh();
        }

        private void ItemStatsRefresh()
        {
            if (SelectedItemIndex != -1)
            {
                bool SameItem = true;
                List<String> tmpStats = Watcher.LoggedItem[SelectedItemIndex].Stats;

                if (tmpStats.Count != listStats.Items.Count)
                {
                    SameItem = false;
                }
                else
                {
                    for (int i = 0; i < listStats.Items.Count; i++)
                    {
                        String stats = (String)listStats.Items[i];
                        if (i < tmpStats.Count)
                        {
                            if (tmpStats[i] != stats)
                            {
                                SameItem = false;
                            }
                        }
                        else
                        {
                            SameItem = false;
                        }
                    }
                }

                if (!SameItem)
                {
                    listStats.Items.Clear();

                    foreach (String str in tmpStats)
                    {
                        listStats.Items.Add(str);
                    }
                }
            }

            listStats.Refresh();
        }

        private void ItemDataGrid_SelectionChanged(object sender, EventArgs e)
        {

            foreach (DataGridViewRow row in ItemDataGrid.SelectedRows)
            {
                SelectedItemIndex = ItemDataGrid.Rows.IndexOf(row);
            }
        }
    }
}
